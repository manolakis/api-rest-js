list = (model, callback) ->
	model.find {}, callback
get = (model, id, callback) ->
	model.findById id, callback
del = (model, id, callback) ->
	model.remove { _id: id }, callback
put = (model, id, data, callback) ->
	model.update { _id:id }, data, { multi: false }, callback
post = (model, data, callback) ->
	new model(data).save callback

module.exports = (app, models) ->

	models.forEach (model) ->
		console.log "modelName: #{model.modelName}"
		app.get "/#{model.modelName}", (req, res) ->
			list model, (err, data) ->
				res.json data

		app.get "/#{model.modelName}/:id", (req, res) ->
			get model, req.params.id, (err, data) ->
				res.json data

		app.post "/#{model.modelName}", (req, res) ->
			post model, req.body, (err, data) ->
				res.json data

		app.del "/#{model.modelName}/:id", (req, res) ->
			del model, req.params, (err, count) ->
				res.json { count: count }

		app.put "/#{model.modelName}/:id", (req, res) ->
			put model, req.params.id, req.body, (err, count) ->
				res.json { count: count }
