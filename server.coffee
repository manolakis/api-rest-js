express = require 'express'
routes = require './routes'
mongoose = require 'mongoose'
dir = "#{__dirname}/dist"
port = process.env.PORT ? process.argv.splice(2)[0] ? 8080
app = express()

mongoose.connect 'mongodb://localhost/test'

model = (name, schema) ->
	mongoose.model name, new mongoose.Schema schema, strict: true

users = model 'users'
	name:
		type: String
		default: ''
	bio:
		type: String
		default: ''
	age:
		type: Number
		default: null

persons = model 'persons'
	name:
		type: String
		default: null

models = [ users, persons ]

app.configure ->
  app.use express.logger 'dev'
  app.use express.bodyParser()
  app.use express.methodOverride()
  app.use express.errorHandler()
  app.use express.static dir
  app.use app.router
  routes app, models

app.listen port, ->
  console.log "started web server at http://localhost:#{port}"