/*global module*/

module.exports = function (grunt) {
    'use strict';

    grunt.initConfig({
        pkg: '<json:package.json>',

        server: {
            app: {
                src: './server.coffee',
                port: 8080,
                watch: './routes.coffee'
            }
        }
    });

    grunt.loadNpmTasks('grunt-hustler');

};